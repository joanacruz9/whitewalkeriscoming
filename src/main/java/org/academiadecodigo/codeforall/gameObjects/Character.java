package org.academiadecodigo.codeforall.gameObjects;

import org.academiadecodigo.codeforall.Game;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Character extends GameObject {

    private boolean isCrashed;
    private CollisionDetector collisionDetector;

    public Character() {
        super();
    }

    public boolean isCrashed() {
        return isCrashed;
    }


    @Override
    public void move() throws InterruptedException {
        super.move();
    }

    public void crash() {
        isCrashed = true;
        super.setSpeed(0);
    }

    // getters & setters
    @Override
    public Picture getPicture() {
        return super.getPicture();
    }

    @Override
    public void setPicture(Picture picture) {
        super.setPicture(picture);

    }


}

