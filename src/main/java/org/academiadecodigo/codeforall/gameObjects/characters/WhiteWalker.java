package org.academiadecodigo.codeforall.gameObjects.characters;

import org.academiadecodigo.codeforall.gameObjects.Character;
import org.academiadecodigo.codeforall.gameObjects.CollisionDetector;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class WhiteWalker extends Character {

    public WhiteWalker() {
        setPicture(new Picture(685, 202, "whitewalker.png"));
    }

    @Override
    public void move() throws InterruptedException {
        // System.out.println("método white walker invocado");
        getPicture().draw();
        getPicture().translate(-45, 0);
        Thread.sleep(100);
    }

}




