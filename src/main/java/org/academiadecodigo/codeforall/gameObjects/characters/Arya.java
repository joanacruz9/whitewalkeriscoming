package org.academiadecodigo.codeforall.gameObjects.characters;

import org.academiadecodigo.codeforall.gameObjects.Character;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Arya extends Character {

    private boolean jumping;
    private Picture movement;

    public Arya() {
        jumping = true;
        setPicture(new Picture(150, 202, "arya.png"));

    }

    public void drawArya() {
        getPicture().draw();
    }

    public void jump() {
        if (jumping) {
            while (getPicture().getY() >= 100 && getPicture().getMaxX() < 635) {
                getPicture().translate(50, -170);
                getPicture().draw();
            }

        }
    }

    public void fall() {
        while (getPicture().getY() < 100) {
            getPicture().translate(50, 170);

        }
    }

    public void moveLeft() {

        if (getPicture().getX() >= 10) {
            getPicture().translate(-5, 0);
        }
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }
}




