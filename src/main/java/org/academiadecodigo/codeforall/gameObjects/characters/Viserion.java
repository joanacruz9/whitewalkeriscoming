package org.academiadecodigo.codeforall.gameObjects.characters;

import org.academiadecodigo.codeforall.gameObjects.Character;
import org.academiadecodigo.codeforall.gameObjects.CollisionDetector;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Viserion extends Character {

    public Viserion() {
        setPicture(new Picture(490, 10, "dragon.png"));
    }

    @Override
    public void move() throws InterruptedException {
        getPicture().draw();
        getPicture().translate(-55, 0);
        Thread.sleep(100);
    }

}