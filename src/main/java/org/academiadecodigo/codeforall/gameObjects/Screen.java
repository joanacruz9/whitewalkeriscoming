package org.academiadecodigo.codeforall.gameObjects;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Screen {

    protected Picture background;

    public void setInitialScreen() {
        background = new Picture(10, 10, "GameStart.png");
        background.draw();
    }

    public void setBackground() {
        background = new Picture(10, 10, "background.jpg");
        background.draw();
    }

    public void setGameWinScreen() {
        background = new Picture(10, 10, "GameWin.jpg");
        background.draw();
    }

    public void setGameOverScreen() {
        background = new Picture(10, 10, "GameOver2.png");
        background.draw();
    }
}
