package org.academiadecodigo.codeforall.gameObjects;

import org.academiadecodigo.codeforall.Game;
import org.academiadecodigo.codeforall.gameObjects.characters.Viserion;
import org.academiadecodigo.codeforall.gameObjects.characters.WhiteWalker;
import org.academiadecodigo.codeforall.gameObjects.characters.Arya;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class CollisionDetector {
    private Arya arya;

    public CollisionDetector(Arya arya) {
        this.arya = arya;

    }

    public boolean check(GameObject gameObject) {
        Picture aryaPicture = arya.getPicture();

        if (gameObject instanceof WhiteWalker) {
            Picture whiteWalkerPicture = gameObject.getPicture();
            if ((aryaPicture.getMaxY() > whiteWalkerPicture.getY()) &&
                    (whiteWalkerPicture.getX() >= aryaPicture.getX() && whiteWalkerPicture.getX() <= aryaPicture.getMaxX())) {

                gameObject.crash();
                Picture crash = new Picture(aryaPicture.getX(),aryaPicture.getY(), "crash.png");
                crash.draw();
                return true;
            }
        }

        if (gameObject instanceof Viserion) {
            Picture viserionPicture = gameObject.getPicture();
            if ((aryaPicture.getMaxY() < viserionPicture.getMaxY()) &&
                    (aryaPicture.getX() <= viserionPicture.getMaxX() && aryaPicture.getX() >= viserionPicture.getMaxY())) {
                gameObject.crash();
                Picture crash = new Picture(aryaPicture.getX(),aryaPicture.getY(), "crash.png");
                crash.draw();
                return true;
            }
        }
        return false;

    }

}


