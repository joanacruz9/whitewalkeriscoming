package org.academiadecodigo.codeforall.gameObjects;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Decor extends GameObject {

    public Decor() {
        setPicture(new Picture(685, 310, "bush.png"));

    }

    @Override
    public void move() throws InterruptedException {
        getPicture().draw();
        while (getPicture().getX() > 0) {
            getPicture().translate(-25, 0);
            Thread.sleep(100);
        }

    }

}










