package org.academiadecodigo.codeforall;

import org.academiadecodigo.codeforall.gameObjects.*;
import org.academiadecodigo.codeforall.gameObjects.characters.Arya;
import org.academiadecodigo.codeforall.gameObjects.characters.Viserion;
import org.academiadecodigo.codeforall.gameObjects.characters.WhiteWalker;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import javax.sound.sampled.Clip;

public class Game implements KeyboardHandler {

    private int delay;
    private Keyboard player;
    private Arya arya;
    private GameObject[] gameObjects;
    private Screen screen;
    private CollisionDetector collisionDetector;
    private int numOfObjects;
    private boolean gameOver;
    private boolean start;
    private boolean gameWin;
    private Sound sound;

    // constructor
    public Game(int delay) {
        this.delay = delay;
        this.player = new Keyboard(this);
        this.arya = new Arya();
        this.numOfObjects = 20;
        this.gameObjects = createGameObjects();
        this.screen = new Screen();
        this.collisionDetector = new CollisionDetector(arya);
        this.start = false;
        this.gameWin = false;
        this.gameOver = false;
    }


    // methods
    public void start() throws InterruptedException {
        screen.setInitialScreen();
        PlaySound.initialSong();

        while (!start) {
            spaceKeyboard();
            System.out.println("Press space to start!");
        }
        PlaySound.getInitial().stop();
        screen.setBackground();
        arya.drawArya();

        while (!gameOver && !gameWin) {
            PlaySound.backgroundSound();
            initKeyboard();
            moveGameObjects(gameObjects);
            arya.jump();
        }

        if (gameOver) {
            screen.setGameOverScreen();
            PlaySound.getBackground().stop();
            PlaySound.deadbitx();
            gameOver = false;
        }

        if (gameWin) {
            screen.setGameWinScreen();
            PlaySound.getBackground().stop();
            PlaySound.winSong();
        }

    }

    // method OK
    public GameObject[] createGameObjects() {
        GameObject[] gameObjects = new GameObject[numOfObjects];
        for (int i = 0; i < numOfObjects; i++) {
            if (i % 2 == 0) {
                gameObjects[i] = new WhiteWalker();
                System.out.println("white walker created");
                continue;
            }
            gameObjects[i] = new Viserion();
            System.out.println("viserion created");
            continue;
        }
        return gameObjects;

    }


    public void moveGameObjects(GameObject[] gameObjects) throws InterruptedException {

        for (GameObject gameObject : gameObjects) {
            if (gameObject instanceof WhiteWalker) {
                while (gameObject.getPicture().getX() > -100) {
                    gameObject.move();
                    if (collisionDetector.check(gameObject)) {
                        gameOver = true;
                        return;
                    }
                }
            }
            if (gameObject instanceof Viserion) {
                while (gameObject.getPicture().getX() > -290) {
                    gameObject.move();
                    if (collisionDetector.check(gameObject)) {
                        gameOver = true;
                        return;
                    }
                }

            }
        }
        gameWin = true;
    }


    public void initKeyboard() {
        KeyboardEvent up = new KeyboardEvent();
        up.setKey(KeyboardEvent.KEY_UP);
        up.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);


        KeyboardEvent right = new KeyboardEvent();
        right.setKey(KeyboardEvent.KEY_RIGHT);
        right.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent left = new KeyboardEvent();
        left.setKey(KeyboardEvent.KEY_LEFT);
        left.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent down = new KeyboardEvent();
        down.setKey(KeyboardEvent.KEY_DOWN);
        down.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        //released
        KeyboardEvent fall = new KeyboardEvent();
        fall.setKey(KeyboardEvent.KEY_UP);
        fall.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);


        player.addEventListener(up);
        player.addEventListener(fall);
        player.addEventListener(left);
        player.addEventListener(right);
        player.addEventListener(down);
    }

    public void spaceKeyboard() {
        KeyboardEvent space = new KeyboardEvent();
        space.setKey(KeyboardEvent.KEY_SPACE);
        space.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        player.addEventListener(space);
    }

    public void keyPressed(KeyboardEvent keyboardEvent) {

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_SPACE) {
            start = true;
        }
        if (keyboardEvent.getKey() == KeyboardEvent.KEY_UP) {
            arya.jump();
        }
        if (keyboardEvent.getKey() == KeyboardEvent.KEY_LEFT) {
            arya.moveLeft();
        }

    }

    public void keyReleased(KeyboardEvent keyboardEvent) {
        if (keyboardEvent.getKey() == KeyboardEvent.KEY_UP) {
            arya.fall();
        }
    }
}















