package org.academiadecodigo.codeforall;

import org.academiadecodigo.codeforall.gameObjects.Sound;

public class PlaySound {
    private static Sound background;
    private static Sound initial;
    private static Sound win;

    public static void initialSong(){
        initial = new Sound("/src/main/resources/initialSong.wav");
        initial.play(true);
    }

    public static void backgroundSound(){
        background = new Sound("/src/main/resources/gameTheme.wav");
        background.play(true);
    }
    public static void deadbitx(){
        background = new Sound("/src/main/resources/deadbitx.wav");
        background.play(true);
    }
    public static void winSong(){
        win = new Sound("/src/main/resources/win.wav");
        win.play(true);
    }

    public static Sound getBackground() {
        return background;
    }

    public static Sound getInitial() {
        return initial;
    }

    public static Sound getWin() {
        return win;
    }
}
