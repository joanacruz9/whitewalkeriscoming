package org.academiadecodigo.codeforall;

public class Main {

    public static void main(String[] args) throws InterruptedException{
        Game game = new Game( 10000);
        game.start();
    }
}
